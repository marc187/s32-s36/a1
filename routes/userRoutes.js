const express = require(`express`);
const router = express.Router();
const userController = require(`./../controllers/userControllers`);
const auth = require("./../auth");

//GET ALL USERS
router.get('/', async (req, res) => {
    try {
        await userController.getAllUsers().then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
     } 
})

//REGISTER A USER
router.post('/register', async (req, res) =>{
    // console.log(req.body) //user object
    try {
        await userController.register(req.body).then(result => res.send(result))
    } catch (error) {
       res.status(500).json(error)
    }
    
})

//CHECK IF EMAIL ALREADY EXIST
router.post(`/email-exists`, async (req, res) => {
    try {
        await userController.checkEmail(req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//LOGIN THE USER
    //AUNTHENTICATION
router.post(`/login`, async (req, res) => {
    // console.log(req.body)

    await userController.login(req.body).then(result => res.send(result))

    // try {
    //     await userController.login(req.body).then(result => res.send(result))
    //     console.log(result)
    // } catch (error) {
    //     res.status(500).json(error)
    // }
})

//RETRIEVE USER INFORMATION
router.get(`/profile`, auth.verify, async (req, res) => {
    const userId = auth.decode(req.headers.authorization).id
    try {
        await userController.profile(userId).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE USER INFORMATION
router.put(`/update`, auth.verify, async (req, res) => {
    const userId = auth.decode(req.headers.authorization).id
    try {
        await userController.update(userId, req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE PASSWORD OF A USER
router.patch(`/update/password`, auth.verify, async (req, res) => {
    // console.log(auth.decode(req.headers.authorization).id)
    const userId = auth.decode(req.headers.authorization).id
    try {
        await userController.updatePassword(userId, req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//SET ADMIN TO TRUE
router.patch(`/isAdmin`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await userController.makeAdmin(req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//SET ADMIN TO FALSE
router.patch(`/isUser`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await userController.notAdmin(req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE
router.delete(`/delete/user`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await userController.deleteUser(req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})


module.exports = router;