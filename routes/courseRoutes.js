const express = require(`express`)
const router = express.Router()
const courseController =  require(`../controllers/courseControllers`)
const auth = require("./../auth");

// GET ALL COURSES
router.get(`/`, async (req, res) => {
    try {
        await courseController.getAllCourses().then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

// CREATE A COURSE
router.post(`/create`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await courseController.createCourse(req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//FIND A COURSE
router.post(`/:courseId`, async (req, res) => {
    try {
        await courseController.findCourse(req.params.courseId).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE A COURSE
router.put(`/:courseId/update`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await courseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//ARCHIVE A COURSE
router.patch(`/:courseId/archive`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await courseController.archiveCourse(req.params.courseId).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//UNARCHIVE A COURSE
router.patch(`/:courseId/unArchive`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await courseController.unArchiveCourse(req.params.courseId).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//FIND ALL ACTIVE COURSES
router.get(`/isActive`, async (req, res) => {
    try {
        await courseController.getAllActiveCourses().then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE COURSE
router.delete(`/:courseId/delete-course`, auth.verifyIfAdmin, async (req, res) => {
    try {
        await courseController.deleteCourse(req.params.courseId).then(result => res.send(result))
    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router