const mongoose = require('mongoose');

// Schema for user
const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        minLength: 3,
        required: [true, `First name is required.`]
    },
    lastName: {
        type: String,
        minLength: 3,
        required: [true, `Last name is required.`]
    },
    email: {
        type: String,
        lowercase: true,
        required: [true, `Email is required.`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required.`]
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    Enrollments: [
        {
            courseId: {
                type: String,
                required: [true, `Course ID is required.`]
            },
            status: {
                type: String,
                default: "Enrolled"
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, {timestamps: true})

module.exports = mongoose.model(`User`, userSchema);