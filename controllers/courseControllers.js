const CryptoJS = require("crypto-js")
const Course = require(`../models/Course`)

// GET ALL COURSES
module.exports.getAllCourses = async () => {
    return await Course.find()
    .then(result => result ? result : error)}

// CREATE A COURSE
module.exports.createCourse = async (reqBody) => {
    const {courseName, description, price} = reqBody
    const newCourse = new Course({courseName, description, price})
    return await newCourse.save()
    .then(result => result ? result : error)}

//FIND A COURSE
module.exports.findCourse = async (courseId) => {
    return await Course.findById(courseId)
    .then(result => result ? result : error)}

//UPDATE A COURSE
module.exports.updateCourse = async (courseId, reqBody) => {
    const courseData = {courseName: reqBody.courseName, description: reqBody.description, price: reqBody.price}
    return await Course.findByIdAndUpdate(courseId, {$set: courseData}, {new:true})
    .then(result => result ? result : error)}

//ARCHIVE A COURSE
module.exports.archiveCourse = async (courseId) => {
    return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: false}}, {new:true})
    .then(result => result ? result : error)}

//UNARCHIVE A COURSE
module.exports.unArchiveCourse = async (courseId) => {
    return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: true}}, {new:true})
    .then(result => result ? result : error)}

//FIND ALL ACTIVE COURSES
module.exports.getAllActiveCourses = async () => {
    return await Course.find({isOffered: true})
    .then(result => result ? result : error)}

//DELETE COURSE
module.exports.deleteCourse = async (courseId) => {
    return await Course.findByIdAndDelete(courseId)
    .then(result => result ? result : error)}