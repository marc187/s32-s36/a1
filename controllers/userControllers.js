const CryptoJS = require("crypto-js");
const auth = require("./../auth");
const User = require(`./../models/User`);

//LOGIN A USER
module.exports.login = async (reqBody) => {
    // console.log(reqBody)
    return await User.findOne({email: reqBody.email}).then((result, error) => {
        if(result == null){
            return false
        } else {
            if(result !== null){
                const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                if(reqBody.password === decryptedPw){
                    return {token: auth.createToken(result)}
                }else{
                    return {auth: `Auth Failed`}
                }
            }else {
                return error
            }
        }
    })
}

//REGISTER A USER
module.exports.register = async (reqBody) => {
    // console.log(reqBody);
    const {firstName, lastName, email, password} = reqBody;
    const newUser = new User({
        firstName,
        lastName,
        email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        // password: bcrypt.hashSync(password, 10)
    })
    return await newUser.save().then(result => {
        if(result){
            return true
        } else {
            if(result == null){
                return false
            }
        }
    })
}

//GET ALL USERS
module.exports.getAllUsers = async () => {
    return await User.find().then(result => result)
}

//CHECK IF EMAIL EXIST
module.exports.checkEmail = async (reqBody) => {
    return await User.findOne({email: reqBody.email}).then( (result, err) => {
        if(result){
            return true
        } else {
            if(result == null){
                return false
            } else{
                return err
            }
        }
    })
}


//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {
    return await User.findById(id).then((result, error) => {
        if(result){
            return result
        }else{
            if(result == null){
                return {message: `User not exist`}
            }else{
                return error
            }
        }
    })
}

//UPDATE USER INFORMATION
module.exports.update = async (userId, reqBody) => {
    const userData = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }
    return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, error) => {
        if(result){
            result.password = "***"
            return result
        }else{
            return error
        }
    })
}

//UPDATE PASSWORD OF A USER
module.exports.updatePassword = async (userId, reqBody) => {
    const newPassword = {
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }
    // console.log(newPassword)
    return await User.findByIdAndUpdate(userId, {$set: newPassword}).then((result, error) => {
        if(result){
            result.password = "***"
            return result
        }else{
            return error
        }
    })
}

//SET ADMIN TO TRUE
module.exports.makeAdmin = async (reqBody) =>{
    return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: true}}, {new:true}).then((result, error) => {
        if(result){
            return true
        }else{
            return error
        }
    })
}

//SET ADMIN TO FALSE
module.exports.notAdmin = async (reqBody) => {
    const {email} = reqBody
    return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}, {new:true}).then((result, error) => {
        if(result){
            return true
        }else{
            return error
        }
    })
}

//DELETE
module.exports.deleteUser = async (reqBody) => {
    return await User.findOneAndDelete({email: reqBody.email}).then(result => result ? result : error)
}
