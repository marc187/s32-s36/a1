const express = require(`express`);
const app = express();
const cors = require(`cors`)

const dotenv = require(`dotenv`);
dotenv.config();

//Middlewares to handle json payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
//prevent blocking of request from client esp different domains
app.use(cors())

//Connect our routes module
const userRoutes = require(`./routes/userRoutes`)
const courseRoutes = require(`./routes/courseRoutes`)

//For database connection using mongoose and mongoDB
const mongoose = require('mongoose');
const { urlencoded } = require('express');
mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once(`open`, () => console.log(`Connected to Database..`));

//Routes
app.use(`/api/users`, userRoutes)
app.use(`/api/courses`, courseRoutes)

const PORT = process.env.PORT || 3000
app.listen(PORT, console.log(`Connected to Server ${PORT}..`));