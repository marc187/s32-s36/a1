const { response } = require("express");
const jwt = require(`jsonwebtoken`);

//SIGN METHOD
module.exports.createToken = (data) => {
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }
    return jwt.sign(userData, process.env.SECRET_PASS)
}

//VERIFY METHOD
module.exports.verify = (req, res, next) => {
    const requestToken = req.headers.authorization
    console.log(requestToken)
    if(typeof requestToken == `undefined`){
        res.status(401).send(`Token missing`)
    }else {
        const token = requestToken.slice(7, requestToken.length)
        if(typeof token !== `undefined`){
            return jwt.verify(token, process.env.SECRET_PASS, (error, data) => {
                // console.log(data)
                if(error){
                    return res.send({auth: `Auth failed`})
                } else{
                    next()
                }
            })
        }
    }
}

//DECODE
module.exports.decode = (bearerToken) => {
    // console.log(bearerToken)
    const token = bearerToken.slice(7, bearerToken.length)
    // console.log(token)
    return jwt.decode(token)
}

//VERIFY IF ADMIN METHOD
module.exports.verifyIfAdmin = (req, res, next) => {
    const requestToken = req.headers.authorization
    console.log(requestToken)
    if(typeof requestToken == `undefined`){
        res.status(401).send(`Token missing`)
    }else {
        const token = requestToken.slice(7, requestToken.length)
        if(typeof token !== `undefined`){
            const admin = jwt.decode(token).isAdmin
            if(admin){
                return jwt.verify(token, process.env.SECRET_PASS, (error, data) => {
                    if(error){
                        return res.send({auth: `Auth failed`})
                    } else{
                        next()
                    }
                })
            }else{
                res.status(403).send(`Your are authorized.`)
            }
            
        }
    }
}